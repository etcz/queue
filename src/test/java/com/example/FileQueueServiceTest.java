package com.example;

import com.google.common.util.concurrent.SettableFuture;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileQueueServiceTest {

  private final static String TEST_DIR = ".file-queue-test";

  @Mock
  private Scheduler scheduler;

  private FileQueueService service;

  @AfterClass
  public static void cleanup() {
    deleteRecursive(TEST_DIR);
  }

  @Before
  public void setUp() throws Exception {
    deleteRecursive(TEST_DIR);
    when(scheduler.schedule(any(), anyLong(), any()))
        .thenReturn(SettableFuture.create());
    when(scheduler.currentTimeMillis())
        .thenReturn(System.currentTimeMillis());
    service = new FileQueueService(scheduler, TEST_DIR, 2, TimeUnit.SECONDS);
  }

  @Test
  public void push_OnEmptyQueue_ElementsAreInOrder() throws Exception {
    String queueUrl = "local://my_queue";

    service.push(queueUrl, "1st");
    service.push(queueUrl, "2nd");
    service.push(queueUrl, "3rd");
    service.push(queueUrl, "4th");

    List<String> messagesFromQueue = extractMessages(service.contents(queueUrl));

    assertEquals(Arrays.asList("1st", "2nd", "3rd", "4th"), messagesFromQueue);
  }

  @Test
  public void push_OnSameQueueFromMultipleServices_YieldsCorrectOrder() throws Exception {
    FileQueueService service1 = new FileQueueService(scheduler, TEST_DIR, 2, TimeUnit.SECONDS);
    FileQueueService service2 = new FileQueueService(scheduler, TEST_DIR, 2, TimeUnit.SECONDS);
    String queueUrl = "local://my_queue";

    service1.push(queueUrl, "from_service_1");
    service2.push(queueUrl, "from_service_2");

    List<String> messagesFromQueue = extractMessages(service1.contents(queueUrl));

    assertEquals(Arrays.asList("from_service_1", "from_service_2"), messagesFromQueue);
  }

  @Test
  public void pull_FromAnEmptyQueue_ReturnsNull() throws Exception {
    String queueUrl = "local://my_queue";
    assertNull(service.pull(queueUrl));
  }

  @Test
  public void pull_FromMultipleQueues_AffectsOnlyTheQueueInQuestion() throws Exception {
    String queueUrl1 = "local://queue_1";
    String queueUrl2 = "local://queue_2";

    service.push(queueUrl1, "q1_1", "q1_2", "q1_3");
    service.push(queueUrl2, "q2_1", "q2_2", "q2_3");
    service.pull(queueUrl1);
    service.pull(queueUrl1);
    List<Record> queue1Contents = service.contents(queueUrl1);
    List<Record> queue2Contents = service.contents(queueUrl2);

    assertEquals(1, queue1Contents.size());
    assertEquals("q1_3", queue1Contents.get(0).getBody());
    assertEquals(3, queue2Contents.size());
  }

  @Test
  public void pull_FromANonEmptyQueue_PutsTheElementBackAtTheHeadAfterTheVisibilityTimeout() throws Exception {
    ManualScheduler scheduler = new ManualScheduler();
    FileQueueService service = new FileQueueService(scheduler, TEST_DIR, 5, TimeUnit.SECONDS);
    String queueUrl = "local://my_queue";

    service.push(queueUrl, "1st");
    service.push(queueUrl, "2nd");
    service.pull(queueUrl);

    scheduler.advanceTimeBy(2, TimeUnit.SECONDS);
    scheduler.triggerActions();

    assertEquals(
        Collections.singletonList("2nd"),
        extractMessages(service.contents(queueUrl))
    );

    scheduler.advanceTimeBy(4, TimeUnit.SECONDS);
    scheduler.triggerActions();

    assertEquals(
        Arrays.asList("1st", "2nd"),
        extractMessages(service.contents(queueUrl))
    );
  }

  @Test
  public void delete_AfterAPullButBeforeTheTimeout_DeletesTheElementPermanently() throws Exception {
    ManualScheduler scheduler = new ManualScheduler();
    FileQueueService service = new FileQueueService(scheduler, TEST_DIR, 10, TimeUnit.SECONDS);
    String queueUrl = "local://my_queue";

    service.push(queueUrl, "1st");
    service.push(queueUrl, "2nd");
    Record element = service.pull(queueUrl);

    scheduler.advanceTimeBy(5, TimeUnit.SECONDS);
    scheduler.triggerActions();

    service.delete(queueUrl, element);

    scheduler.advanceTimeBy(6, TimeUnit.SECONDS);
    scheduler.triggerActions();

    assertEquals(
        Collections.singletonList("2nd"),
        extractMessages(service.contents(queueUrl))
    );
  }

  @Test
  public void delete_AfterAPullButAfterTheTimeout_HasNoEffect() throws Exception {
    ManualScheduler scheduler = new ManualScheduler();
    FileQueueService service = new FileQueueService(scheduler, TEST_DIR, 5, TimeUnit.SECONDS);
    String queueUrl = "local://my_queue";

    service.push(queueUrl, "1st");
    service.push(queueUrl, "2nd");
    service.push(queueUrl, "3rd");
    Record element = service.pull(queueUrl);

    scheduler.advanceTimeBy(6, TimeUnit.SECONDS);
    scheduler.triggerActions();

    service.delete(queueUrl, element);

    assertEquals(
        Arrays.asList("1st", "2nd", "3rd"),
        extractMessages(service.contents(queueUrl))
    );
  }

  private static void deleteRecursive(String dir) {
    try {
      Files.walk(Paths.get(dir))
          .sorted(Comparator.reverseOrder())
          .map(Path::toFile)
          .forEach(File::delete);
    } catch (Throwable ignored) {
    }
  }

  private List<String> extractMessages(List<Record> records) {
    return records.stream()
        .map(Record::getBody)
        .collect(toList());
  }
}
