package com.example;

import com.google.common.util.concurrent.SettableFuture;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class InMemoryQueueServiceTest {

  @Mock
  private Scheduler scheduler;

  private InMemoryQueueService service;

  @Before
  public void setUp() {
    when(scheduler.schedule(any(), anyLong(), any()))
        .thenReturn(SettableFuture.create());
    service = new InMemoryQueueService(scheduler, 2, TimeUnit.SECONDS);
  }

  @Test
  public void push_OnEmptyQueue_ElementsAreInOrder() {
    String queueUrl = "local://my_queue";

    service.push(queueUrl, "1st");
    service.push(queueUrl, "2nd");
    service.push(queueUrl, "3rd");
    service.push(queueUrl, "4th");

    List<String> messagesFromQueue = extractMessages(service.contents(queueUrl));

    assertEquals(Arrays.asList("1st", "2nd", "3rd", "4th"), messagesFromQueue);
  }

  @Test
  public void push_ToDifferentQueues_RestrictsVisibility() {
    String queueUrl1 = "local://queue_1";
    String queueUrl2 = "local://queue_2";
    String messageForQueue1 = "this_message_is_for_queue_1";
    String messageForQueue2 = "this_message_is_for_queue_2";

    service.push(queueUrl1, messageForQueue1);
    service.push(queueUrl2, messageForQueue2);

    List<String> queue1Contents = extractMessages(service.contents(queueUrl1));
    List<String> queue2Contents = extractMessages(service.contents(queueUrl2));

    assertTrue(queue1Contents.contains(messageForQueue1));
    assertFalse(queue1Contents.contains(messageForQueue2));
    assertTrue(queue2Contents.contains(messageForQueue2));
    assertFalse(queue2Contents.contains(messageForQueue1));
  }

  @Test
  public void pull_FromANonEmptyQueue_RemovesTheHeadImmediately() {
    String queueUrl = "local://my_queue";

    service.push(queueUrl, "1st_element");
    service.pull(queueUrl);

    assertEquals(0, service.contents(queueUrl).size());
  }

  @Test
  public void pull_FromMultipleQueues_AffectsOnlyTheQueueInQuestion() {
    String queueUrl1 = "local://queue_1";
    String queueUrl2 = "local://queue_2";

    service.push(queueUrl1, "q1_1", "q1_2", "q1_3");
    service.push(queueUrl2, "q2_1", "q2_2", "q2_3");
    service.pull(queueUrl1);
    service.pull(queueUrl1);
    List<Record> queue1Contents = service.contents(queueUrl1);
    List<Record> queue2Contents = service.contents(queueUrl2);

    assertEquals(1, queue1Contents.size());
    assertEquals("q1_3", queue1Contents.get(0).getBody());
    assertEquals(3, queue2Contents.size());
  }

  @Test
  public void pull_FromAnEmptyQueue_ReturnsNull() {
    String queueUrl = "local://my_queue";
    assertNull(service.pull(queueUrl));
  }

  @Test
  public void pull_FromANonEmptyQueue_PutsTheElementBackAtTheHeadAfterTheVisibilityTimeout() {
    ManualScheduler scheduler = new ManualScheduler();
    InMemoryQueueService service = new InMemoryQueueService(scheduler, 5, TimeUnit.SECONDS);
    String queueUrl = "local://my_queue";

    service.push(queueUrl, "1st");
    service.push(queueUrl, "2nd");
    service.pull(queueUrl);

    scheduler.advanceTimeBy(2, TimeUnit.SECONDS);
    scheduler.triggerActions();

    assertEquals(
        Collections.singletonList("2nd"),
        extractMessages(service.contents(queueUrl))
    );

    scheduler.advanceTimeBy(4, TimeUnit.SECONDS);
    scheduler.triggerActions();

    assertEquals(
        Arrays.asList("1st", "2nd"),
        extractMessages(service.contents(queueUrl))
    );
  }

  @Test
  public void delete_AfterAPullButBeforeTheTimeout_DeletesTheElementPermanently() {
    ManualScheduler scheduler = new ManualScheduler();
    InMemoryQueueService service = new InMemoryQueueService(scheduler, 10, TimeUnit.SECONDS);
    String queueUrl = "local://my_queue";

    service.push(queueUrl, "1st");
    service.push(queueUrl, "2nd");
    Record element = service.pull(queueUrl);

    scheduler.advanceTimeBy(5, TimeUnit.SECONDS);
    scheduler.triggerActions();

    service.delete(queueUrl, element);

    scheduler.advanceTimeBy(6, TimeUnit.SECONDS);
    scheduler.triggerActions();

    assertEquals(
        Collections.singletonList("2nd"),
        extractMessages(service.contents(queueUrl))
    );
  }

  @Test
  public void delete_AfterAPullButAfterTheTimeout_HasNoEffect() {
    ManualScheduler scheduler = new ManualScheduler();
    InMemoryQueueService service = new InMemoryQueueService(scheduler, 5, TimeUnit.SECONDS);
    String queueUrl = "local://my_queue";

    service.push(queueUrl, "1st");
    service.push(queueUrl, "2nd");
    service.push(queueUrl, "3rd");
    Record element = service.pull(queueUrl);

    scheduler.advanceTimeBy(6, TimeUnit.SECONDS);
    scheduler.triggerActions();

    service.delete(queueUrl, element);

    assertEquals(
        Arrays.asList("1st", "2nd", "3rd"),
        extractMessages(service.contents(queueUrl))
    );
  }

  private List<String> extractMessages(List<Record> records) {
    return records.stream()
        .map(Record::getBody)
        .collect(toList());
  }
}
