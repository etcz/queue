package com.example;

import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.UUID;

public final class Record {
  public static final String separator = ":";
  private final String id;
  private final String receiptHandle;
  private final String body;

  private Record(String id, String receiptHandle, String body) {
    this.id = id;
    this.receiptHandle = receiptHandle;
    this.body = body;
  }

  public Record(Record other) {
    this.id = other.id;
    this.receiptHandle = other.receiptHandle;
    this.body = other.body;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("body", body)
        .toString();
  }

  public String getId() {
    return id;
  }

  public String getReceiptHandle() {
    return receiptHandle;
  }

  public String getBody() {
    return body;
  }

  public String encode() {
    return Joiner.on(separator).join(id, receiptHandle, body);
  }

  public static Record decode(String s) {
    List<String> l = Lists.newArrayList(Splitter.on(separator).split(s));
    return new Record(l.get(0), l.get(1), l.get(2));
  }

  public static Record create(String body) {
    String uuid1 = UUID.randomUUID().toString();
    String uuid2 = UUID.randomUUID().toString();
    String receiptHandle = uuid2.replaceAll("-", "");
    return new Record(uuid1, receiptHandle, body);
  }

  public static Record create(String receiptHandle, String body) {
    String uuid1 = UUID.randomUUID().toString();
    return new Record(uuid1, receiptHandle, body);
  }
}
