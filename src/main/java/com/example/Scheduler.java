package com.example;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public interface Scheduler {
  Future<?> schedule(Runnable runnable, long delay, TimeUnit unit);

  Future<?> scheduleAtFixedRate(Runnable runnable, long period, TimeUnit unit);

  long currentTimeMillis();

  void shutdown();
}
