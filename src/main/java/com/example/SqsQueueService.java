package com.example;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class SqsQueueService implements QueueService {
  private final AmazonSQSClient sqsClient;

  public SqsQueueService(AmazonSQSClient sqsClient) {
    this.sqsClient = sqsClient;
  }

  @Override
  public void push(String queueUrl, String... messages) {
    List<SendMessageBatchRequestEntry> entries = new ArrayList<>();
    for (int i = 0; i < messages.length; i++) {
      SendMessageBatchRequestEntry entry = new SendMessageBatchRequestEntry()
          .withId(String.valueOf(i))
          .withMessageBody(messages[i]);
      entries.add(entry);
    }
    SendMessageBatchRequest batchRequest = new SendMessageBatchRequest()
        .withQueueUrl(queueUrl)
        .withEntries(entries);
    sqsClient.sendMessageBatch(batchRequest);
  }

  @Override
  public Record pull(String queueUrl) {
    ReceiveMessageRequest request = new ReceiveMessageRequest(queueUrl)
        .withMaxNumberOfMessages(1);
    List<Message> messages = sqsClient.receiveMessage(request)
        .getMessages();
    if (messages.isEmpty()) {
      return null;
    }
    Message head = messages.get(0);
    return Record.create(head.getReceiptHandle(), head.getBody());
  }

  @Override
  public void delete(String queueUrl, Record record) {
    DeleteMessageRequest request = new DeleteMessageRequest(queueUrl, record.getReceiptHandle());
    sqsClient.deleteMessage(request);
  }

  @Override
  public List<Record> contents(String queueUrl) {
    ReceiveMessageRequest request = new ReceiveMessageRequest(queueUrl)
        .withMaxNumberOfMessages(10);
    List<Message> messages = sqsClient.receiveMessage(request)
        .getMessages();
    return messages.stream()
        .map(m -> Record.create(m.getReceiptHandle(), m.getBody()))
        .collect(toList());
  }

  @Override
  public void shutdown() {
    sqsClient.shutdown();
  }
}
