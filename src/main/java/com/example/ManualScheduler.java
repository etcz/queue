package com.example;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.SettableFuture;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public final class ManualScheduler implements Scheduler {
  private long timeNowMillis;
  private final Map<Runnable, Long> tasksScheduleMap;
  private final Map<PeriodicRunnable, Long> periodicTasksScheduleMap;

  public ManualScheduler() {
    this(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
  }

  public ManualScheduler(long timeNow, TimeUnit unit) {
    this.timeNowMillis = unit.toMillis(timeNow);
    tasksScheduleMap = new ConcurrentHashMap<>();
    periodicTasksScheduleMap = new ConcurrentHashMap<>();
  }

  @Override
  public Future<?> schedule(Runnable runnable, long delay, TimeUnit unit) {
    tasksScheduleMap.put(runnable, timeNowMillis + unit.toMillis(delay));
    SettableFuture<Object> future = SettableFuture.create();
    Futures.addCallback(future, new FutureCallback<Object>() {
      @Override
      public void onSuccess(@Nullable Object o) {
        future.set(o);
      }

      @Override
      public void onFailure(@Nonnull Throwable t) {
        tasksScheduleMap.remove(runnable);
      }
    });
    return future;
  }

  @Override
  public Future<?> scheduleAtFixedRate(Runnable runnable, long period, TimeUnit unit) {
    PeriodicRunnable periodicRunnable = new PeriodicRunnable(runnable, period, unit);
    long nextSchedule = timeNowMillis + periodicRunnable.periodToMillis();
    SettableFuture<Object> future = SettableFuture.create();
    periodicTasksScheduleMap.put(periodicRunnable, nextSchedule);
    Futures.addCallback(future, new FutureCallback<Object>() {
      @Override
      public void onSuccess(@Nullable Object o) {
        future.set(o);
      }

      @Override
      public void onFailure(@Nonnull Throwable t) {
        periodicTasksScheduleMap.remove(periodicRunnable);
      }
    });
    return future;
  }

  @Override
  public long currentTimeMillis() {
    return timeNowMillis;
  }

  @Override
  public void shutdown() {
    tasksScheduleMap.clear();
  }

  public void reset(long value, TimeUnit unit) {
    this.timeNowMillis = unit.toMillis(value);
  }

  public void advanceTimeBy(long duration, TimeUnit unit) {
    timeNowMillis += unit.toMillis(duration);
  }

  public void triggerActions() {
    tasksScheduleMap.entrySet().removeIf(e -> {
      Runnable runnable = e.getKey();
      long scheduledTime = e.getValue();
      boolean hasElapsed = timeNowMillis >= scheduledTime;
      if (hasElapsed) {
        runnable.run();
      }
      return hasElapsed;
    });

    periodicTasksScheduleMap.entrySet().forEach(e -> {
      PeriodicRunnable runnable = e.getKey();
      long scheduledTime = e.getValue();
      boolean hasElapsed = timeNowMillis >= scheduledTime;
      if (hasElapsed) {
        runnable.run();
        long nextSchedule = timeNowMillis + runnable.periodToMillis();
        e.setValue(nextSchedule);
      }
    });
  }

  private static final class PeriodicRunnable implements Runnable {
    final Runnable runnable;
    final long period;
    final TimeUnit timeUnit;

    PeriodicRunnable(Runnable runnable, long period, TimeUnit timeUnit) {
      this.runnable = runnable;
      this.period = period;
      this.timeUnit = timeUnit;
    }

    long periodToMillis() {
      return timeUnit.toMillis(period);
    }

    @Override
    public void run() {
      runnable.run();
    }
  }
}
