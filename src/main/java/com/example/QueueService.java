package com.example;

import java.io.IOException;
import java.util.List;

public interface QueueService {
  void push(String queueUrl, String... messages) throws InterruptedException, IOException;

  Record pull(String queueUrl) throws InterruptedException, IOException;

  void delete(String queueUrl, Record record) throws InterruptedException, IOException;

  List<Record> contents(String queueUrl) throws InterruptedException, IOException;

  void shutdown();
}
