package com.example;

import com.google.common.base.Preconditions;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class InMemoryQueueService implements QueueService {
  private final Map<String, InMemoryQueue> queueMap;
  private final Scheduler scheduler;
  private final long visibilityTimeout;
  private final TimeUnit timeUnit;
  private boolean isShutdown;

  public InMemoryQueueService(Scheduler scheduler, int visibilityTimeout, TimeUnit timeUnit) {
    this.scheduler = scheduler;
    this.visibilityTimeout = visibilityTimeout;
    this.timeUnit = timeUnit;
    queueMap = new ConcurrentHashMap<>();
    isShutdown = false;
  }

  @Override
  public void push(String queueUrl, String... messages) {
    Preconditions.checkNotNull(queueUrl);
    Preconditions.checkState(!isShutdown);

    InMemoryQueue queue = queueMap.computeIfAbsent(
        queueUrl,
        k -> new InMemoryQueue(scheduler, visibilityTimeout, timeUnit)
    );
    queue.push(messages);
  }

  @Override
  public Record pull(String queueUrl) {
    Preconditions.checkNotNull(queueUrl);
    Preconditions.checkState(!isShutdown);

    InMemoryQueue queue = queueMap.get(queueUrl);
    return queue != null ? queue.pull() : null;
  }

  @Override
  public void delete(String queueUrl, Record record) {
    Preconditions.checkNotNull(queueUrl);
    Preconditions.checkNotNull(record);
    Preconditions.checkState(!isShutdown);

    InMemoryQueue queue = queueMap.get(queueUrl);
    if (queue != null) {
      queue.delete(record);
    }
  }

  @Override
  public List<Record> contents(String queueUrl) {
    Preconditions.checkNotNull(queueUrl);

    InMemoryQueue queue = queueMap.get(queueUrl);
    return queue != null ? queue.contents() : Collections.emptyList();
  }

  @Override
  public void shutdown() {
    scheduler.shutdown();
    isShutdown = true;
  }

  private static class InMemoryQueue {
    final Deque<Record> records;
    final Map<Record, Future<?>> recordFutureMap;
    final Scheduler scheduler;
    final long visibilityTimeout;
    final TimeUnit timeUnit;

    InMemoryQueue(Scheduler scheduler, long visibilityTimeout, TimeUnit timeUnit) {
      this.scheduler = scheduler;
      this.visibilityTimeout = visibilityTimeout;
      this.timeUnit = timeUnit;
      records = new ConcurrentLinkedDeque<>();
      recordFutureMap = new ConcurrentHashMap<>();
    }

    synchronized void push(String... messages) {
      for (String message : messages) {
        records.add(Record.create(message));
      }
    }

    Record pull() {
      Record head = records.poll();
      Runnable addToFront = () -> records.addFirst(head);
      recordFutureMap.put(head, scheduler.schedule(addToFront, visibilityTimeout, timeUnit));
      return head;
    }

    void delete(Record record) {
      Future<?> visibilityFuture = recordFutureMap.get(record);
      visibilityFuture.cancel(false);
      recordFutureMap.remove(record);
    }

    List<Record> contents() {
      return new ArrayList<>(records);
    }
  }
}
