package com.example;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class QueueServiceDemo {

  private static void runQueueDemo(QueueService queueService, String queueUrl, int numClients, int timeLimit) throws Exception {
    CountDownLatch latch = new CountDownLatch(numClients);
    ExecutorService clientExecutor = Executors.newFixedThreadPool(numClients);
    ExecutorService monitorExecutor = Executors.newSingleThreadExecutor();

    // Start queue monitor
    QueueMonitor qm = new QueueMonitor(queueService, queueUrl, timeLimit, TimeUnit.SECONDS, latch);
    monitorExecutor.submit(qm);

    // Let those clients run loose!
    List<DemoClient> clients = new ArrayList<>(numClients);
    for (int i = 0; i < numClients; i++) {
      String tag = "client_" + (i + 1);
      RandomClient rc = new RandomClient(tag, queueService, queueUrl, timeLimit, TimeUnit.SECONDS, latch);
      clients.add(rc);
    }
    clients.forEach(clientExecutor::submit);

    // Sync point, shutdown-all-the-things
    latch.await();
    clientExecutor.shutdown();
    monitorExecutor.shutdown();
    queueService.shutdown();
  }

  private static void runInMemoryQueueDemo(int numClients, int runTime, int visibilityTimeout, TimeUnit timeUnit) throws Exception {
    String queueUrl = "inmem://queue1";
    AutomaticScheduler queueScheduler = new AutomaticScheduler();
    QueueService service = new InMemoryQueueService(queueScheduler, visibilityTimeout, timeUnit);

    runQueueDemo(service, queueUrl, numClients, runTime);
  }

  private static void runFileQueueDemo(String demoDir, int numClients, int runTime, int visibilityTimeout, TimeUnit timeUnit) throws Exception {
    String queueUrl = "file://queue1";
    AutomaticScheduler queueScheduler = new AutomaticScheduler();
    QueueService service = new FileQueueService(queueScheduler, demoDir, visibilityTimeout, timeUnit);

    runQueueDemo(service, queueUrl, numClients, runTime);
  }

  private static void runAwsDemo(Regions region) {
    ProfileCredentialsProvider credentialsProvider = new ProfileCredentialsProvider();
    credentialsProvider.getCredentials();
    AmazonSQSClient sqsClient = new AmazonSQSClient(credentialsProvider)
        .withRegion(region);
    SqsQueueService service = new SqsQueueService(sqsClient);

    CreateQueueRequest request = new CreateQueueRequest("first-queue-test");
    String queueUrl = sqsClient.createQueue(request).getQueueUrl();

    service.push(queueUrl, "test-message");
    Record record = service.pull(queueUrl);
    if (service.pull(queueUrl) != null) {
      System.out.println("Message body: " + record.getBody());
    }
    service.shutdown();
  }

  private static void deleteRecursive(String dir) {
    try {
      Files.walk(Paths.get(dir))
          .sorted(Comparator.reverseOrder())
          .map(Path::toFile)
          .forEach(File::delete);
    } catch (Throwable ignored) {
    }
  }

  public static void main(String[] args) {
    try {
      System.out.println("\n---------------------------------------");
      System.out.println("In-memory queue service demo");
      System.out.println("---------------------------------------");
      runInMemoryQueueDemo(2, 10, 2, TimeUnit.SECONDS);

      System.out.println("\n---------------------------------------");
      System.out.println("File queue service demo");
      System.out.println("---------------------------------------");
      String demoDir = ".demo";
      deleteRecursive(demoDir);
      runFileQueueDemo(demoDir, 2, 10, 2, TimeUnit.SECONDS);
      deleteRecursive(demoDir);

      System.out.println("\n---------------------------------------");
      System.out.println("SQS adapter demo");
      System.out.println("---------------------------------------");
      runAwsDemo(Regions.AP_SOUTHEAST_2);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}