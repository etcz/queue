package com.example;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class AutomaticScheduler implements Scheduler {
  private final ScheduledExecutorService service;

  public AutomaticScheduler() {
    this(Runtime.getRuntime().availableProcessors());
  }

  public AutomaticScheduler(int threadPoolSize) {
    this(Executors.newScheduledThreadPool(threadPoolSize));
  }

  public AutomaticScheduler(ScheduledExecutorService service) {
    this.service = service;
  }

  @Override
  public long currentTimeMillis() {
    return System.currentTimeMillis();
  }

  @Override
  public Future<?> schedule(Runnable runnable, long delay, TimeUnit unit) {
    return service.schedule(runnable, delay, unit);
  }

  @Override
  public Future<?> scheduleAtFixedRate(Runnable runnable, long period, TimeUnit unit) {
    return service.scheduleAtFixedRate(runnable, 0, period, unit);
  }

  public void shutdown() {
    service.shutdown();
  }
}