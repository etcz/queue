package com.example;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class FileQueueService implements QueueService {
  private final Scheduler scheduler;
  private final String baseDir;
  private final long visibilityTimeout;
  private final TimeUnit timeUnit;
  private final Map<String, InFlightHandler> queueInFlightHandlerMap;
  private boolean isShutdown;

  public FileQueueService(Scheduler scheduler, String baseDir, long visibilityTimeout, TimeUnit timeUnit)
      throws IOException {
    this.scheduler = scheduler;
    this.baseDir = baseDir;
    this.visibilityTimeout = visibilityTimeout;
    this.timeUnit = timeUnit;
    queueInFlightHandlerMap = new ConcurrentHashMap<>();
    isShutdown = false;
    Path base = Paths.get(baseDir);
    if (!Files.exists(base)) {
      Files.createDirectory(base);
    }
  }

  @Override
  public void push(String queueUrl, String... messages) throws InterruptedException, IOException {
    Preconditions.checkNotNull(queueUrl);
    Preconditions.checkState(!isShutdown);

    String queueName = createQueue(queueUrl);
    File messagesFile = getMessagesFile(queueName);
    File queueLock = getLockFile(queueName);
    FileWriter fw = new FileWriter(messagesFile, true);

    startInFlightHandler(queueName);

    acquireLock(queueLock);
    try (PrintWriter pw = new PrintWriter(fw)) {
      for (String message : messages) {
        Record r = Record.create(message);
        FileQueueRecord fqr = new FileQueueRecord(0, r);
        pw.println(fqr.encode());
      }
    } finally {
      releaseLock(queueLock);
    }
  }

  @Override
  public Record pull(String queueUrl) throws InterruptedException, IOException {
    Preconditions.checkNotNull(queueUrl);
    Preconditions.checkState(!isShutdown);

    String queueName = createQueue(queueUrl);
    File messagesFile = getMessagesFile(queueName);
    File messagesCopy = getMessagesCopy(queueName);
    File queueLock = getLockFile(queueName);
    File inFlightFile = getInFlightFile(queueName);
    File inFlightLock = getInFlightLock(queueName);
    FileWriter messagesCopyFw = new FileWriter(messagesCopy, false);
    FileWriter inFlightFw = new FileWriter(inFlightFile, true);
    Record record;

    acquireLock(queueLock);
    acquireLock(inFlightLock);
    try (PrintWriter messagesCopyPw = new PrintWriter(messagesCopyFw);
         PrintWriter inFlightPw = new PrintWriter(inFlightFw)) {

      // Filter out records that are in-flight
      List<String> lines = Files.lines(messagesFile.toPath())
          .filter(l -> {
            FileQueueRecord fqr = FileQueueRecord.decode(l);
            return fqr.inFlightTime == 0;
          })
          .collect(toList());

      // Check if queue is empty (or if all records are in-flight)
      String firstLine = lines.get(0);
      if (firstLine == null) {
        record = null;
      } else {
        // Mark first line as in-flight and timestamp it
        FileQueueRecord original = FileQueueRecord.decode(firstLine);
        long now = scheduler.currentTimeMillis();
        FileQueueRecord inFlightRecord = new FileQueueRecord(now, original.record);

        // Copy the current record to the temp file and the in-flight file
        messagesCopyPw.println(inFlightRecord.encode());
        inFlightPw.println(inFlightRecord.encode());

        // Copy the rest of the lines to temp file
        lines.listIterator(1)
            .forEachRemaining(messagesCopyPw::println);

        // Overwrite previous queue file
        mv(messagesCopy, messagesFile);

        record = original.record;
      }
    } catch (Throwable ignored) {
      record = null;
    } finally {
      releaseLock(inFlightLock);
      releaseLock(queueLock);
    }

    return record;
  }

  @Override
  public void delete(String queueUrl, Record record) throws InterruptedException, IOException {
    Preconditions.checkNotNull(queueUrl);
    Preconditions.checkNotNull(record);
    Preconditions.checkState(!isShutdown);

    String queueName = fromUrl(queueUrl);
    File messagesFile = getMessagesFile(queueName);
    File messagesCopy = getMessagesCopy(queueName);
    File queueLock = getLockFile(queueName);
    File inFlightFile = getInFlightFile(queueName);
    File inFlightCopy = getInFlightCopy(queueName);
    File inFlightLock = getInFlightLock(queueName);
    FileWriter messagesCopyFw = new FileWriter(messagesCopy, false);
    FileWriter inFlightCopyFw = new FileWriter(inFlightCopy, false);

    acquireLock(queueLock);
    acquireLock(inFlightLock);
    try (PrintWriter messagesCopyPw = new PrintWriter(messagesCopyFw);
         PrintWriter inFlightCopyPw = new PrintWriter(inFlightCopyFw);
         Stream<String> messagesLines = Files.lines(messagesFile.toPath());
         Stream<String> inFlightLines = Files.lines(inFlightFile.toPath())) {

      // Copy all of the lines, except for the record in question
      // Only delete the record if it's in-flight
      messagesLines.filter(l -> {
        FileQueueRecord fqr = FileQueueRecord.decode(l);
        boolean sameRecord = fqr.record.getId().equals(record.getId());
        boolean isInFlight = fqr.inFlightTime > 0;
        return !sameRecord || !isInFlight;
      }).forEach(messagesCopyPw::println);

      // Overwrite previous queue file
      mv(messagesCopy, messagesFile);

      // Delete the record from the in-flight file as well
      inFlightLines.filter(l -> {
        FileQueueRecord fqr = FileQueueRecord.decode(l);
        return !fqr.record.getId().equals(record.getId());
      }).forEach(inFlightCopyPw::println);

      // Overwrite in flight file
      mv(inFlightCopy, inFlightFile);

    } finally {
      releaseLock(inFlightLock);
      releaseLock(queueLock);
    }
  }

  @Override
  public List<Record> contents(String queueUrl) throws InterruptedException, IOException {
    Preconditions.checkNotNull(queueUrl);

    String queueName = createQueue(queueUrl);
    File messagesFile = getMessagesFile(queueName);
    File queueLock = getLockFile(queueName);

    acquireLock(queueLock);
    try (Stream<String> lines = Files.lines(messagesFile.toPath())) {
      // Filter out records that are in-flight
      Stream<Record> records = lines.filter(l -> {
        FileQueueRecord fqr = FileQueueRecord.decode(l);
        return fqr.inFlightTime == 0;
      }).map(l -> {
        FileQueueRecord fqr = FileQueueRecord.decode(l);
        return fqr.record;
      });
      return records.collect(toList());
    } finally {
      releaseLock(queueLock);
    }
  }

  @Override
  public void shutdown() {
    scheduler.shutdown();
    isShutdown = true;
  }

  private String createQueue(String queueUrl) {
    String domain = null;
    try {
      domain = new URI(queueUrl).getAuthority();
      Files.createDirectory(Paths.get(baseDir, domain));
    } catch (FileAlreadyExistsException ignored) {
    } catch (Throwable t) {
      throw new RuntimeException(t);
    }
    return domain;
  }

  private String fromUrl(String queueUrl) {
    try {
      return new URI(queueUrl).getAuthority();
    } catch (Throwable t) {
      throw new RuntimeException(t);
    }
  }

  private void startInFlightHandler(String queueName) {
    queueInFlightHandlerMap.computeIfAbsent(
        queueName,
        k -> {
          InFlightHandler handler = new InFlightHandler(queueName);
          scheduler.scheduleAtFixedRate(handler,  1, TimeUnit.SECONDS);
          return handler;
        }
    );
  }

  private File getMessagesFile(String queueName) {
    return getRelativeFile(queueName, "messages");
  }

  private File getMessagesCopy(String queueName) {
    return getRelativeFile(queueName, "messages_copy");
  }

  private File getLockFile(String queueName) {
    return getRelativeFile(queueName, "lock");
  }

  private File getInFlightFile(String queueName) {
    return getRelativeFile(queueName, "in_flight");
  }

  private File getInFlightCopy(String queueName) {
    return getRelativeFile(queueName, "in_flight_copy");
  }

  private File getInFlightLock(String queueName) {
    return getRelativeFile(queueName, "in_flight_lock");
  }

  private File getRelativeFile(String queueName, String fileName) {
    String path = Paths.get(queueName, fileName).toString();
    return new File(baseDir, path);
  }

  private void acquireLock(File lock) throws InterruptedException {
    while (!lock.mkdir()) {
      Thread.sleep(50);
    }
  }

  private void releaseLock(File lock) {
    lock.delete();
  }

  private void mv(File src, File dest) throws IOException {
    Path srcPath = src.toPath();
    Path destPath = dest.toPath();
    Files.move(srcPath, destPath, StandardCopyOption.REPLACE_EXISTING);
  }

  private static class FileQueueRecord {
    final long inFlightTime;
    final Record record;

    FileQueueRecord(long inFlightTime, Record record) {
      this.inFlightTime = inFlightTime;
      this.record = record;
    }

    public String encode() {
      return Joiner.on(Record.separator).join(inFlightTime, record.encode());
    }

    public static FileQueueRecord decode(String s) {
      Splitter splitter = Splitter.on(Record.separator).limit(2);
      List<String> tokens = Lists.newArrayList(splitter.split(s));
      long inFlightTime = Long.parseLong(tokens.get(0));
      Record record = Record.decode(tokens.get(1));
      return new FileQueueRecord(inFlightTime, record);
    }
  }

  private class InFlightHandler implements Runnable {
    final String queueName;
    volatile boolean locksAcquired;

    InFlightHandler(String queueName) {
      this.queueName = queueName;
      locksAcquired = false;
    }

    private List<Record> updateInFlightItems(File inFlightFile, File inFlightCopy) throws IOException {
      Preconditions.checkState(locksAcquired);

      FileWriter inFlightCopyFw = new FileWriter(inFlightCopy, false);
      List<Record> notInFlight = new ArrayList<>();

      try (Stream<String> lines = Files.lines(inFlightFile.toPath());
           PrintWriter inFlightCopyPw = new PrintWriter(inFlightCopyFw)) {
        // If the in-flight item has passed its visibility timeout,
        // then it needs to be removed from the file
        lines.forEach(l -> {
          FileQueueRecord fqr = FileQueueRecord.decode(l);
          long visibilityTime = fqr.inFlightTime + timeUnit.toMillis(visibilityTimeout);
          long timeNow = scheduler.currentTimeMillis();
          if (timeNow >= visibilityTime) {
            notInFlight.add(fqr.record);
          } else {
            inFlightCopyPw.println(fqr.encode());
          }
        });
      }
      // Overwrite to get rid of the soon-to-be-visible items
      mv(inFlightCopy, inFlightFile);

      return notInFlight;
    }

    private void makeVisible(List<Record> records, File messagesFile, File messagesCopy) throws IOException {
      Preconditions.checkState(locksAcquired);

      FileWriter messagesCopyFw = new FileWriter(messagesCopy, false);

      try (Stream<String> lines = Files.lines(messagesFile.toPath());
           PrintWriter messagesCopyPw = new PrintWriter(messagesCopyFw)) {
        // For each record we need to make visible,
        // reset its timestamp to 0 to indicate that it's no longer in-flight
        lines.forEach(l -> {
          FileQueueRecord fqr = FileQueueRecord.decode(l);
          Optional<Record> match = records.stream()
              .filter(r -> fqr.record.getId().equals(r.getId()))
              .findFirst();
          if (match.isPresent()) {
            // Reset timestamp (i.e. item is no longer in-flight)
            FileQueueRecord newRecord = new FileQueueRecord(0, match.get());
            messagesCopyPw.println(newRecord.encode());
          } else {
            messagesCopyPw.println(l);
          }
        });
      }
      // Restore newly visible items
      mv(messagesCopy, messagesFile);
    }

    @Override
    public void run() {
      File inFlightFile = getInFlightFile(queueName);
      if (!inFlightFile.exists() || inFlightFile.length() == 0) {
        return;
      }

      File inFlightCopy = getInFlightCopy(queueName);
      File inFlightLock = getInFlightLock(queueName);
      File messagesFile = getMessagesFile(queueName);
      File messagesCopy = getMessagesCopy(queueName);
      File queueLock = getLockFile(queueName);

      try {
        acquireLock(queueLock);
        acquireLock(inFlightLock);
        locksAcquired = true;
        List<Record> notInFlight = updateInFlightItems(inFlightFile, inFlightCopy);
        makeVisible(notInFlight, messagesFile, messagesCopy);
      } catch (Throwable ignored) {
      } finally {
        releaseLock(inFlightLock);
        releaseLock(queueLock);
        locksAcquired = false;
      }
    }
  }
}
