package com.example;

import com.google.common.base.Joiner;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

abstract class DemoClient implements Runnable {
  final QueueService service;
  final String queueUrl;
  final int allowedRuntime;
  final TimeUnit timeUnit;
  final CountDownLatch latch;

  DemoClient(QueueService service, String queueUrl, int allowedRuntime, TimeUnit timeUnit, CountDownLatch latch) {
    this.service = service;
    this.queueUrl = queueUrl;
    this.allowedRuntime = allowedRuntime;
    this.timeUnit = timeUnit;
    this.latch = latch;
  }

  abstract void doAction() throws Exception;

  @Override
  public void run() {
    long timeLimit = timeUnit.toSeconds(allowedRuntime);
    long timeTaken = 0;
    do {
      try {
        doAction();
        sleep(1000);
        timeTaken++;
      } catch (Throwable ignored) {

      }
    } while (timeTaken < timeLimit);
    latch.countDown();
  }
}

final class RandomClient extends DemoClient {
  final String tag;

  RandomClient(String tag, QueueService service, String queueUrl, int allowedRuntime, TimeUnit timeUnit, CountDownLatch latch) {
    super(service, queueUrl, allowedRuntime, timeUnit, latch);
    this.tag = tag;
  }

  private boolean coinFlip() {
    return new Random().nextBoolean();
  }

  private void log(String action, String message) {
    String prefix = "[" + tag + "]:";
    System.out.println(Joiner.on(" ").join(prefix, action, message));
  }

  @Override
  void doAction() throws Exception {
    if (coinFlip()) {
      String ts = String.valueOf(System.currentTimeMillis());
      service.push(queueUrl, ts);
      log("PUSH", ts);
    } else {
      Record head = service.pull(queueUrl);
      log("PULL", head.toString());
      if (coinFlip()) {
        service.delete(queueUrl, head);
        log("DELETE", head.toString());
      }
    }
  }
}

final class QueueMonitor extends DemoClient {
  QueueMonitor(QueueService service, String queueUrl, int allowedRuntime, TimeUnit timeUnit, CountDownLatch latch) {
    super(service, queueUrl, allowedRuntime, timeUnit, latch);
  }

  private void log(String toDisplay) {
    System.out.println(toDisplay);
  }

  private void log(List<Record> records) {
    String prefix = "[" + queueUrl + "]:";
    log(prefix + " " + toCsv(records));
  }

  private String toCsv(List<Record> records) {
    return Joiner.on(",").join(records);
  }

  @Override
  void doAction() throws Exception {
    log(service.contents(queueUrl));
  }
}